# Create your views here.
from django.http import HttpResponse
from django.shortcuts import render_to_response
from rights.utils import secure_view
from django.contrib.auth.models import User,Group, Permission
from django.core.context_processors import csrf
from smartphone.models import Room
import json
import simplejson
from pylab import *

import scipy.signal as signal
import numpy
import django
import os

@secure_view
def login(request):
	return render_to_response('pages/login.html')

@secure_view
def home(request):
	return render_to_response('pages/home.html',{"rooms":Room.objects.all()})

@secure_view
def user(request):
	print request.user
	return render_to_response('pages/user.html')

@secure_view
def rights(request,what = None,what_id = None):
	if request.user.is_staff:
		return render_to_response('pages/rights.html',{"perms":Permission.objects.all(),"rooms":Room.objects.all(),"type_id":what_id,"users":User.objects.all(),"groups":Group.objects.all(),"csrf_token":unicode(csrf(request)['csrf_token']),"type":what,"user":request.user})
	else:
		return render_to_response('pages/rights.html',{"perms":Permission.objects.all(),"rooms":Room.objects.all(),"type_id":request.user.pk,"csrf_token":unicode(csrf(request)['csrf_token']),"type":"user"})
		

@secure_view
def setFilter(request,fe=44100):
	nyquist = fe/2.
	c = {"values":[]}
	with open("filtercoeffs.json","r") as f:
		data = simplejson.loads(f.read())
		i = 0
		nb_bandes = data["nb_bande"]
		for v in data["values"]:
			c["values"].append({"bande": str(i*nyquist/nb_bandes)+" - "+ str((i+1)*nyquist/nb_bandes) + " Hz","value":v})
			i = i+1
	c.update({"csrf_token":unicode(csrf(request)['csrf_token'])})
	return render_to_response('pages/filter.html',c)

#Plot frequency and phase response
def mfreqz(fig,b,a=1):
    w,h = signal.freqz(b,a)
    h_dB = 20 * log10 (abs(h))
    ax = fig.add_subplot(211)
    ax.plot(w/max(w),h_dB)
    ax.set_ylim(-50, 5)
    ax.set_ylabel('Magnitude (db)')
    ax.set_xlabel(r'Normalized Frequency (x$\pi$rad/sample)')
    ax.set_title(r'Frequency response')
    ax = fig.add_subplot(212)
    h_Phase = unwrap(arctan2(imag(h),real(h)))
    ax.plot(w/max(w),h_Phase)
    ax.set_ylabel('Phase (radians)')
    ax.set_xlabel(r'Normalized Frequency (x$\pi$rad/sample)')
    ax.set_title(r'Phase response')
    fig.subplots_adjust(hspace=0.5)
"""

#Plot step and impulse response
def impz(fig,b,a=1):
    l = len(b)
    impulse = repeat(0.,l); impulse[0] =1.
    x = arange(0,l)
    response = signal.lfilter(b,a,impulse)
    ax = fig.subplot(211)
    ax.stem(x, response)
    ax.ylabel('Amplitude')
    ax.xlabel(r'n (samples)')
    ax.title(r'Impulse response')
    ax = fig.subplot(212)
    step = cumsum(response)
    ax.stem(x, step)
    ax.ylabel('Amplitude')
    ax.xlabel(r'n (samples)')
    ax.title(r'Step response')
    ax.subplots_adjust(hspace=0.5)"""

def bandpass(f1,f2,n=251):
	a = signal.firwin(n, cutoff = f1, window = 'blackmanharris')
	b = - signal.firwin(n, cutoff = f2, window = 'blackmanharris'); b[n/2] = b[n/2] + 1
	d = - (a+b); d[n/2] = d[n/2] + 1
	return d

@secure_view
def changeValue(request,fe=44100):
	from matplotlib.backends.backend_agg import FigureCanvasAgg as FigureCanvas
	from matplotlib.figure import Figure
	with open("filtercoeffs.json","r") as f:
		data = simplejson.loads(f.read())
		fig = Figure()
		n = 81
		nb_bandes = 10
		values = request.POST.getlist("data[]")
		for i in range(len(values)):
			values[i] = 10.**(float(values[i])/10.)
		#coeffs = [1,0.5,0.2,0.9,0.000001,1,0.5,0.2,0.9,0.01,1,0.5,0.2,0.9,0.01,1,0.5,0.2,0.9,0.01]
		width = 0.5/nb_bandes
		a = values[0]*numpy.array(bandpass(0.001,width,n=n)) #Fe=200000 4kHz-6Khz
		bande = width
		i = 1
		while bande + width < 0.5:
			a += values[i]*numpy.array(bandpass(bande,bande+width,n=n))
			bande += width
			i += 1
		mfreqz(fig,a)
	canvas=FigureCanvas(fig)
	response=django.http.HttpResponse(content_type='plain/text')
	with open("filtercoeffs.json","w") as f:
		data["values"] = values
		data["coeffs"] = a.tolist()
		f.write(simplejson.dumps(data))
	with open("tmp","wb") as f:
		canvas.print_png(f)
	with open("tmp","rb") as f:
		contents = f.read().encode("base64").replace("\n", "")
	os.remove("tmp")
	return HttpResponse('data:image/png;base64,' + contents)
	
