from django.contrib.auth.models import User
from django.db import models
from django import forms

class Order(models.Model):
	user 		= models.ForeignKey(User)
	last_loc	= models.ForeignKey('Room')
	current_music	= models.CharField(max_length=200)
	finished	= models.BooleanField(default=False)
	frames 		= models.IntegerField(default=0)
	frame_type_ms	= models.BooleanField(default=False)
	new		= models.BooleanField()

class Room(models.Model):
	name 		= models.CharField(max_length=200,unique=True)
	short_name	= models.CharField(max_length=200,unique=True)
	ip 		= models.CharField(max_length=15,unique=False)
	port 		= models.IntegerField(default=2222)
	pipe_name 	= models.CharField(max_length=200,unique=True)

class GeoLoc(models.Model):
	room 		= models.ForeignKey('Room')
	user 		= models.ForeignKey(User)

