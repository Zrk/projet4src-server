from django.http import HttpResponse
from django.shortcuts import render_to_response

from django.contrib.auth.models import User
from django.contrib.auth import authenticate
from django.contrib.auth import login
from django.shortcuts import redirect

from django.views.decorators.csrf import csrf_exempt


from smartphone.models import GeoLoc,Room,Order
from log.models import Log
#from smartphone.utils import AllowRoomChange

import os
import simplejson

"""@csrf_exempt
def infos(request):
	if  not request.POST.has_key('username') and not request.POST.has_key('password'): # no connection
		return HttpResponse(simplejson.dumps({"error":"not logged"}))
	
	username = request.POST['username']
	password = request.POST['password']
	user = authenticate(username=username, password=password)
	if user == None:
		return HttpResponse(simplejson.dumps({"error":"not logged"}))
	
	answer = {"error":""}	
	data = simplejson.loads(request.POST['data'])
	cmd = {"Volume" : data["volume"],"playlist":""}
	geoloc = GeoLoc.objects.filter(user_id=user.id).all()[0]
	
	if geoloc.room.id != data["room"]: #user has moved
		oldroom = geoloc.room

		geoloc.room = Room.objects.filter(id=data["room"]).all()[0]
		geoloc.save()
		
		print "User: "+user.username + " has moved from room " + oldroom.name + " to room " + geoloc.room.name
		Log(value="User: "+user.username + " has moved from room " + oldroom.name + " to room " + geoloc.room.name,log_type='I').save()
		if oldroom.id != 1:
			fd = os.open("pipes/"+oldroom.pipe_name,os.O_RDWR)
			error = True
			while error:
				try:
					with open("times/"+oldroom.short_name+".time","r") as f:
						l = f.readline()
						print "LINE",l
						frames = int(l)
						cmd.update({"frames": frames})
						print "frames",frames
				except ValueError:
					pass
				else:
					error = False
			cmd["playlist"] = [] #nothing to play, user is not in the room
			os.write(fd,simplejson.dumps(cmd))
			os.close(fd)
	cmd["playlist"] = data["playlist"]
	
	if user.has_perm("rights.can_play_"+geoloc.room.short_name.lower()):
		if geoloc.room.id != 1:
			fd = os.open("pipes/"+geoloc.room.pipe_name,os.O_RDWR)
			os.write(fd,simplejson.dumps(cmd))
			os.close(fd)
	else:
		answer["error"] = "You are not allowed to play music in this room."
	
	return HttpResponse(simplejson.dumps(answer))"""

@csrf_exempt
def infos(request):
	try:
		data = simplejson.loads(request.body)
	except:
		data = {}
		for k,v in request.POST.iteritems():
			data[k] = v
	print "DATA",data
	if not data.has_key('username') and not data.has_key('password'): # no connection
		print "not connected"
		return HttpResponse(simplejson.dumps({"error":"not logged"}))
	
	username = data['username']
	password = data['password']
	user = authenticate(username=username, password=password)
	answer = {"error":"","show":False}
	if user == None:
		print "ERROR","not logged"
		return HttpResponse(simplejson.dumps({"error":"not logged"}))
	
	
	geoloc = GeoLoc.objects.get(user__id=user.id)
	#update the localisation
	if data.has_key('geoloc'):
		rooms_ids = {"a":2,"b":3,"c":4,"d":5}
		try:
			geoloc.room = Room.objects.get(pk=rooms_ids[data["geoloc"]])
		except:
			answer["error"] = "room does not exit"
			return HttpResponse(simplejson.dumps(answer))
		geoloc.save()
		
	if data.has_key("playlist") and not data["playlist"] == "":
		tmp =  data["playlist"].split('/')
		tmp = tmp[-2] + os.sep + tmp[-1]

	if user.has_perm("rights.can_play_"+geoloc.room.short_name.lower()):
		if geoloc.room.id != 1:
			if data.has_key("playlist"):
				if not data["playlist"] == "":
					tmp =  data["playlist"].split('/')
					tmp = tmp[-2] + os.sep + tmp[-1]
				else:
					tmp = data["playlist"]
				print "to read",tmp
				order = Order()
				order.user = user
				order.current_music = tmp
				order.last_loc = Room.objects.get(geoloc__user=user)
				order.new = True
				if data.has_key("frames"):
					order.frames = data["frames"]
				if data.has_key("frames") and data.has_key("frames_type_ms") and data["frames_type_ms"] == "true":
					order.frame_type_ms = True
				order.save()
	else:
		answer["error"] = "You are not allowed to play music in this room."
		answer["show"] = True
	
	if data.has_key("volume"):
		if user.has_perm("rights.can_change_volume_"+geoloc.room.short_name.lower()):
			fd = os.open("pipes/"+geoloc.room.pipe_name,os.O_RDWR)
			os.write(fd,simplejson.dumps({"volume":int(data["volume"])/15.}))
			os.close(fd)
		else:
			answer["error"] = "You are not allowed to change the volume in this room."
			answer["show"] = True
	if answer["error"] == "":
		print "ERROR",answer["error"]
	return HttpResponse(simplejson.dumps(answer))
	
@csrf_exempt
def getPos(request):
	data = {"pos":[]}
	
	for g in GeoLoc.objects.all():
		data["pos"].append({"user":g.user.username,"room":g.room.short_name})
	
	return HttpResponse(simplejson.dumps(data))
	
