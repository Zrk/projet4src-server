#!/usr/bin/env python
# -*- coding: utf-8 -*-

from django.contrib.auth.models import User,Group, Permission
from smartphone.models import GeoLoc,Room,Order
from rights.models import Rights

from django.contrib.contenttypes.models import ContentType

import os

for r in Order.objects.all():
	r.delete()

for r in Room.objects.all():
	r.delete()

for r in GeoLoc.objects.all():
	r.delete()

for r in User.objects.all():
	r.delete()

for r in Group.objects.all():
	r.delete()

for r in Permission.objects.all():
	r.delete()

for r in ContentType.objects.all():
	r.delete()

childrens = Group(name = "Childrens")
parents = Group(name = "Parents")
parents.save()
childrens.save()

enf1 = User.objects.create_user('enfant1', 'maison@maison.com', 'enfant1')
enf1.set_password('enfant1')
enf1.groups.add(childrens)
enf1.save()

enf2 = User.objects.create_user('enfant2', 'maison@maison.com', 'enfant2')
enf2.set_password('enfant2')
enf2.groups.add(childrens)
enf2.save()

par1 = User.objects.create_user('parent1', 'maison@maison.com', 'parent1')
par1.set_password('parent1')
par1.groups.add(parents)
par1.save()

par2 = User.objects.create_user('parent2', 'maison@maison.com', 'parent2')
par2.set_password('parent2')
par2.groups.add(parents)
par2.save()


rooms = [
#		["BedRoomChildren1","Chambre 1",[enf1],[enf1,parents],"192.168.1.100",2222],
		["BedRoomChildren2","Chambre 2",[enf2],[enf2,parents],"192.168.1.101",2222],
#		["BedRoomParent","Chambre parents",[parents],[parents],"192.168.1.102",2222],
		["kitchen","Cuisine",[parents],[parents],"192.168.1.100",2222]
	]

o = Room()
o.ip = ""
o.name = "Outside"
o.short_name = "Outside"
o.pipe_name = "/dev/null"
o.save()

c = ContentType.objects.get_for_model(Rights)

for room in rooms:
	r = Room()
	r.ip = room[4]
	r.port = room[5]
	r.name = room[1]
	r.short_name = room[0].lower()
	r.pipe_name = room[0].lower()+".pipe"
	os.system("mkfifo pipes/"+r.pipe_name)
	r.save()

	p_play = Permission.objects.create(codename='can_play_'+r.short_name.lower(),name=u'Autorisation de jouer de la musique dans la pièce: ' + r.name,content_type=c)
	p_change_volume = Permission.objects.create(codename='can_change_volume_'+r.short_name.lower(),name=u'Autorisation de changer le volume dans la pièce: ' + r.name,content_type=c)
	
	for can_play_entity in room[2]:
		if type(can_play_entity) == Group:
			can_play_entity.permissions.add(p_play)
		elif type(can_play_entity) == User:
			can_play_entity.user_permissions.add(p_play)
		can_play_entity.save()
	
	for can_change_volume_entity in room[3]:
		if type(can_change_volume_entity) == Group:
			can_change_volume_entity.permissions.add(p_change_volume)
		elif type(can_change_volume_entity) == User:
			can_change_volume_entity.user_permissions.add(p_change_volume)
		can_change_volume_entity.save()
	
for e in User.objects.all():
	g = GeoLoc()
	g.user = e
	g.room = o
	g.save()

#run tabs
cmd = "gnome-terminal --tab -e \"bash -c 'cd ~/work/projet4src-server/server';'sudo python manage.py runserver 192.168.1.12:80';bash\" "
cmd += " --tab -e \"bash -c 'cd ~/work/projet4src-server/server';'python manage.py CheckCommandes';bash\" "
for room in rooms:
	cmd += "--tab -e \"bash -c 'cd ~/work/projet4src-module/';'echo python run.py ./module_streaming -ip "+room[4]+" -port "+str(room[5])+" -room "+room[0].lower()+" -rl -local';bash\" "
	cmd += "--tab -e \"bash -c 'ssh pi@"+room[4]+"';bash\" "
os.system(cmd + " &")
exit()
