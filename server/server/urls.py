from django.conf.urls import patterns, include, url

# Uncomment the next two lines to enable the admin:
# from django.contrib import admin
# admin.autodiscover()

urlpatterns = patterns('',
    url(r'^$', 'web.views.home'),
    url(r'login', 'rights.utils.login'),
    url(r'^logout/$', 'rights.utils.logout_view'),
    url(r'home', 'web.views.home'),
    url(r'smartphone', 'smartphone.views.infos'),
    url(r'^logs$', 'log.views.logs_page'),

    url(r'^getPos$', 'smartphone.views.getPos'),

    url(r'^getlogs$', 'log.views.getlogs'),
    url(r'^setlog$', 'log.views.setlog'),
    url(r'^rights/(?P<what>\w+)/(?P<what_id>\d+)/$', 'web.views.rights'),
    url(r'^rights/$', 'web.views.rights'),
    url(r'^getRight/(?P<codename>\w+)/(?P<what>\w+)/(?P<what_id>\d+)/$', 'rights.utils.getRight'),
    url(r'^changeRight/(?P<codename>\w+)/(?P<what>\w+)/(?P<what_id>\d+)/$', 'rights.utils.changeRight'),
    url(r'^profil/$', 'web.views.setFilter'),
    url(r'^changeValue/$', 'web.views.changeValue'),

    # Examples:
    # url(r'^$', 'server.views.home', name='home'),
    # url(r'^server/', include('server.foo.urls')),

    # Uncomment the admin/doc line below to enable admin documentation:
    # url(r'^admin/doc/', include('django.contrib.admindocs.urls')),

    # Uncomment the next line to enable the admin:
    # url(r'^admin/', include(admin.site.urls)),
)
