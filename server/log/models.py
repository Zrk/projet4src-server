from django.db import models

# Create your models here.

class Log(models.Model):
	date = models.DateTimeField(auto_now_add=True, blank=True)
	value = models.CharField(max_length=200)

	LOG_TYPES = (
		('I', 'Info'),
		('W', 'Warning'),
		('E', 'Error'),
	)
	
	log_type = models.CharField(max_length=1, choices=LOG_TYPES)

