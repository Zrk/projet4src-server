from django.http import HttpResponse
from django.shortcuts import render_to_response

from django.contrib.auth.models import User
from django.contrib.auth import authenticate
from django.contrib.auth import login
from django.shortcuts import redirect

from django.views.decorators.csrf import csrf_exempt

from smartphone.models import GeoLoc,Room
from log.models import Log
#from smartphone.utils import AllowRoomChange

import os
import simplejson
from .models import Log
import dateutil.parser
import datetime

@csrf_exempt
def infos(request):
	if  not request.POST.has_key('username'):
		return HttpResponse(simplejson.dumps({"error":"not logged"}))

def logs_page(request):
	return render_to_response('pages/log.html')

@csrf_exempt
def setlog(request):
	print request.POST
	if request.POST.has_key('value') and request.POST.has_key('from') and request.POST.has_key('type'):
		Log(value="["+request.POST['from']+"] " + request.POST["value"],log_type=request.POST['type']).save()
	return HttpResponse("ok")

@csrf_exempt
def getlogs(request):
	logs = None
	if request.POST.has_key('date') and request.POST['date'] != "":
		last_date = dateutil.parser.parse(request.POST['date'])
		logs = Log.objects.filter(date__gt=last_date).all()[:10]
	else:
		logs = Log.objects.all()

	data = {"logs":[]}

	for l in logs:
		data["logs"].append({"date":l.date.isoformat(),"value":l.value,"type":l.log_type})

	return HttpResponse(simplejson.dumps(data))
