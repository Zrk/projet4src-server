from django.core.management.base import BaseCommand, CommandError
from smartphone.models import *
import time,glob,os,simplejson

class Command(BaseCommand):
	def handle(self, *args, **options):
		while True:
			time.sleep(0.3) #300ms
			# check for new commande
			for order in Order.objects.filter(new=True):
				print "New incomming commande from " + order.user.username
				for last_order in Order.objects.all().filter(new=False):
				#for last_order in Order.objects.filter(user=order.user).filter(new=False):
					# duplicate
					#if last_order.current_music != order.current_music or order.last_loc != last_order.last_loc:
					print "  stop previous command"
					fd = os.open("pipes/"+last_order.last_loc.pipe_name,os.O_RDWR)
					os.write(fd,simplejson.dumps({"current_music":""}))
					os.close(fd)
					last_order.delete()
				order.new = False
				loc = GeoLoc.objects.get(user=order.user)
				#check if he is allowed
				if order.user.has_perm("rights.can_play_"+loc.room.short_name.lower()):
					# send command to module
					fd = os.open("pipes/"+loc.room.pipe_name,os.O_RDWR)
					if not order.frames == 0:
						if order.frame_type_ms:
							os.write(fd,simplejson.dumps({"current_music":order.current_music,"frames":order.frames,"frames_type_ms":True}))
						else:
							os.write(fd,simplejson.dumps({"current_music":order.current_music,"frames":order.frames}))
					else:
						os.write(fd,simplejson.dumps({"current_music":order.current_music}))
					os.close(fd)

				order.last_loc = loc.room
				order.save()
			
			#check if some users has moved
			for order in Order.objects.all():
				loc = GeoLoc.objects.get(user=order.user)
				if order.last_loc != loc.room and order.user.has_perm("rights.can_play_"+loc.room.short_name.lower()):
					fd = os.open("pipes/"+order.last_loc.pipe_name,os.O_RDWR)
					os.write(fd,simplejson.dumps({"current_music":""}))
					os.close(fd)
					cmd = {}
					# retrieve the pos in the file
					err = True
					while err:
						try:
							with open("times/"+order.last_loc.pipe_name[:-5]+".time","r") as f:
								l = f.readline()
								frames = int(l)
								if frames != -1:
									cmd.update({"frames":frames})
								else:
									cmd.update({"frames":0})
							err=False
						except:
							pass
					cmd.update({"current_music":order.current_music})
					fd = os.open("pipes/"+loc.room.pipe_name,os.O_RDWR)
					os.write(fd,simplejson.dumps(cmd))
					os.close(fd)
					order.last_loc = loc.room
					order.save()
			
			"""#check for modules that need something to do
			for pipe_name in glob.glob("times/*.time"):
				try:
					with open(pipe_name,"r") as f:
						l = f.readline()
						frames = int(l)
						if frames == -1: # need data
							print "Delete completed order"
							order = Order.objects.filter(last_loc__name=pipe_name.split("/")[-1][:-5])
							print order
							order.delete()
							if len(order) == 0:
								continue
							order = order[0]
							if len(order.music_set.all()) != 0:
								order.current_music = order.music_set.all()[0]
								order.music_set.all()[0].order = None
							order.save()
							fd = os.open("pipes/"+order.last_loc.pipe_name,os.O_RDWR)
							os.write(fd,simplejson.dumps({"current_music":order.current_music.music}))
							os.close(fd)
				except ValueError:
					pass"""

