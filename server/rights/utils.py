from django.shortcuts import render_to_response
from django.contrib.auth.models import User,Group,Permission
from django.template import RequestContext
from django.contrib.auth import authenticate, login,logout
from django.shortcuts import redirect
from django.http import HttpResponse
import simplejson

from django.shortcuts import get_list_or_404, get_object_or_404

def secure_view(func):
	def secure_view_func(request,*args, **kwargs):	
		if request.POST.has_key("password") and request.POST.has_key("username"):
			username = request.POST['username']
			password = request.POST['password']
			user = authenticate(username=username, password=password)
			if user is not None:
				if user.is_active:
					login(request, user)
					return func(request,*args, **kwargs)
			else:
				return render_to_response('log.html',context_instance=RequestContext(request))
		elif not request.user.is_authenticated():
			return render_to_response('log.html',context_instance=RequestContext(request))
		else:
			return func(request,*args, **kwargs)
	return secure_view_func

def logout_view(request):
	logout(request)
	return render_to_response('log.html',context_instance=RequestContext(request))

def changeRight(request,codename,what,what_id):
	print "request",request.GET["right"]
	if not request.user.is_staff:
		return HttpResponse("not allowed")	
	if what == "user":
		user = User.objects.get(pk=what_id)
		if request.GET["right"] == "true":
			user.user_permissions.add(get_object_or_404(Permission,codename=codename))
		else:
			user.user_permissions.remove(get_object_or_404(Permission,codename=codename))
		user.save()
	elif what == "group":
		group = get_object_or_404(Group,pk=what_id)
		if request.GET["right"] == "true":
			group.permissions.add(get_object_or_404(Permission,codename=codename))
		else:
			group.permissions.remove(get_object_or_404(Permission,codename=codename))
		group.save()
	return HttpResponse("OK")

def getRight(request,codename,what,what_id):
	if what == "user":
		user = User.objects.get(pk=what_id)
		return HttpResponse(simplejson.dumps({"resp":user.has_perm("rights."+codename),"can_changed":True if request.user.is_staff and (get_object_or_404(Permission,codename=codename) in user.user_permissions.all() or not user.has_perm("rights."+codename)) else False}))
	elif what == "group":
		group = get_object_or_404(Group,pk=what_id)
		perm = get_object_or_404(Permission,codename=codename)
		if not group == None or not perm == None:
			r = perm.group_set.filter(id=group.id)
			if len(r) == 1:
				return HttpResponse(simplejson.dumps({"resp":True}))
			else:
				return HttpResponse(simplejson.dumps({"resp":False}))
		else:
			return HttpResponse(simplejson.dumps({"resp":False}))
	return HttpResponse("ERROR")
	
